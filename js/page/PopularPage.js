import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  // createStackNavigator,
  createMaterialTopTabNavigator,
  createAppContainer
} from 'react-navigation';
import NavigationUtil from '../navigator/NavigationUtil';
import DetailPage from './DetailPage';


type Props = {};
export default class PopularPage extends Component<Props> {

  renderTabNavigator(){
    const tabNavigator=createMaterialTopTabNavigator({
      PopularTab1:{
        screen:PopularTab,
        navigationOptions:{
          title:'Tab1'
        }
      },
      PopularTab2:{
        screen:PopularTab,
        navigationOptions:{
          title:'Tab2'
        }
      },
      PopularTab3:{
        screen:PopularTab,
        navigationOptions:{
          title:'Tab3'
        }
      },
      PopularTab4:{
        screen:PopularTab,
        navigationOptions:{
          title:'Tab4'
        }
      },
    });
    return createAppContainer(tabNavigator);
  }
  render() {
    const TabNavigator=this.renderTabNavigator();
     return <View style={{flex:1,marginTop:40}}><TabNavigator/></View>;
  }
}

class PopularTab extends Component<Props> {
  render() {
    const {tabLabel}=this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>{tabLabel}</Text>
        <Text onPress={()=>{NavigationUtil.goPage({navigation:this.props.navigation},'DetailPage')}}>跳转到详情页</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  tabStyle:{
    marginVertical: 20,
  }
});
