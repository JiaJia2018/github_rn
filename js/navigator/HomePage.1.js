
import {
  createBottomTabNavigator,
  createAppContainer
} from 'react-navigation';

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PopularPage from '../page/PopularPage';
import TrendingPage from '../page/TrendingPage';
import FavoritePage from '../page/FavoritePage';
import MyPage from '../page/MyPage';
import NavigationUtil from './NavigationUtil';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';



type Props = {};
export default class HomePage extends Component<Props> {
  _tabNavigator(){
    const tab = createBottomTabNavigator({
      PopularPage:{
        screen:PopularPage,
        navigationOptions:{
          tabBarLabel:"最热",
          tabBarIcon:({tintColor,focused})=>{
                return <MaterialIcons 
                name={'whatshot'}
                size={26}
                style={{color:tintColor}}
                />
          }
        }
      },
      TrendingPage:{
        screen:TrendingPage,
        navigationOptions:{
          tabBarLabel:"趋势",
          tabBarIcon:({tintColor,focused})=>{
            return <Ionicons 
            name={'md-trending-up'}
            size={26}
            style={{color:tintColor}}
            />
      }
        }
      },
      FavoritePage:{
        screen:FavoritePage,
        navigationOptions:{
          tabBarLabel:"收藏",
          tabBarIcon:({tintColor,focused})=>{
            return <MaterialIcons 
            name={'favorite'}
            size={26}
            style={{color:tintColor}}
            />
      }
        }
      },
      MyPage:{
        screen:MyPage,
        navigationOptions:{
          tabBarLabel:"我的",
          tabBarIcon:({tintColor,focused})=>{
            return <Entypo 
            name={'user'}
            size={26}
            style={{color:tintColor}}
            />
      }
        }
      },
      });
      return createAppContainer(tab);
  }

  render() {
    NavigationUtil.navigation = this.props.navigation; //存储最外层的Navigator即ppNavigator，页面内部实现跳转到最外层Navigator里面的页面的时候
    const Tab = this._tabNavigator();
    return <Tab/>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
